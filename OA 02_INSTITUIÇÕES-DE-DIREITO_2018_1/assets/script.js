
/*
	       OA TEMPLATE 4.0
	----------------------------
	Desenvolvido em CoffeeScript

		Código: Fabiane Lima
 */

(function() {
  var imgs, preload;

  imgs = ['assets/img/img1.png', 'assets/img/img2.png', 'assets/img/img3.png', 'assets/img/img4.png', 'assets/img/img5.png'];

  preload = function(imgs) {
    var counter;
    counter = 0;
    $(imgs).each(function() {
      $('<img />').attr('src', this).appendTo('body').css({
        display: 'none'
      });
      return counter++;
    });
    if (counter === imgs.length) {
      $('main').css({
        opacity: '1'
      });
      return $('body').css({
        background: '#e7e7e7'
      });
    }
  };

  $(window).on('load', function() {
    return preload(imgs);
  });

  $(function() {
    var anima, clique, count, data, func, ini;
    ini = 0;
    count = 0;
    anima = [0, -100, -200, -300, -400];
    clique = new Audio('assets/clique.mp3');
    data = [
      {
        tit: 'Validade da lei',
        txt: ['A lei é uma regra escrita, de força obrigatória, cujo descumprimento gera uma sanção. Devem ser observadas algumas condições quanto à forma e quanto à matéria da lei para saber se ela é válida:', '<strong>Ótica Formal</strong><br/>A validade sob a ótica formal depende do processo de criação da lei, que precisa estar em conformidade com o devido processo legislativo previsto na Constituição Federal. Se assim não for, ela não será válida.', '<strong>Ótica Material</strong><br/>A validade sob a ótica material refere-se à matéria, ao assunto tratado pela lei. Assim, existem assuntos que não podem ser objeto da lei. Também alguns assuntos são restritos a determinado ente da federação, então, se uma lei for aprovada pelo Estado do Paraná e tratar de assunto de competência do Município de Curitiba, a lei não será válida.']
      }, {
        tit: 'Vigência da lei',
        txt: ['Uma lei deve ser elaborada, promulgada e publicada. Ao ser elaborada, a lei é encaminhada para o legislativo (quem aprova uma lei) por meio de um projeto de lei.', 'Depois ela é submetida a aprovação daquele órgão que irá votar o projeto. Se o projeto de lei for aprovado, passa a ser uma lei.', 'Algumas leis têm tanta repercussão na vida das pessoas que não podem produzir efeitos logo que são publicadas, ou seja, não podem passar a ter vigência imediatamente.', 'A lei é encaminhada para o executivo (aquele que promulga e publica a lei). Só depois de publicada é que a lei passa a ser obrigatória para todas as pessoas.']
      }, {
        tit: 'Eficácia da lei',
        txt: ['Enquanto a lei não está em vigor, ou seja, no período de vacância, “a lei como ato jurídico, existe, é válida, mas não é eficaz. A eficácia da lei, isto é, seus efeitos plenos, só ocorrem com sua entrada em vigor” (VENOSA, 2012, p. 108).', 'Imagine que uma lei publicada em 1 de janeiro de 2015 tenha no seu corpo um artigo que determina que ela entrará em vigor em 1 ano. Neste caso, a lei teria vacatio legis expressa.', 'Diante dessa situação, a lei, a partir do dia 2 de janeiro de 2015, é válida, mas não é eficaz, porque não está vigente ainda.']
      }, {
        tit: 'Revogação da lei',
        txt: ['Uma lei válida torna-se eficaz, ou seja, passa a produzir efeitos quando entra em vigor. Mas até quando ela ficará vigente?', 'Até que outra lei a revogue ou a modifique, ela ainda estará produzindo efeitos no mundo jurídico.', 'Denomina-se ab-rogação quando a lei é revogada integralmente e derrogação quando é revogada parcialmente.']
      }, {
        tit: 'Princípio da irretroatividade da lei',
        txt: ['Uma vez em vigor, a lei terá plena eficácia, total aplicabilidade, mas a Constituição Federal, no art. 5º, inciso XXXVI, garante que uma lei nova não pode prejudicar o direito adquirido, ato jurídico perfeito e coisa julgada (VENOSA, 2012).', 'Se uma pessoa se aposentou com 60 anos em janeiro de 2015 e em fevereiro do mesmo ano uma lei nova determinou que a idade mínima para se aposentar é com no mínimo 70 anos, como fica a situação da pessoa que já se aposentou com menos idade? Ela continuará aposentada porque já tinha direito adquirido.']
      }
    ];
    func = {
      info: function() {
        clique.play();
        return $('.modal').html('<h1>Referência</h1><p>VENOSA, Sílvio de Salvo. <b>Direito civil</b>. 12. ed. São Paulo: Atlas, 2012. v. 1.</p><button class="end">Voltar</button>');
      },
      end: function() {
        clique.play();
        $('.content').fadeOut();
        $('.dimmer').delay(200).fadeIn();
        return $('.modal').html('<h1>Finalizando...</h1><p>Recapitulamos algumas características importantes das leis, principal fonte do direito, sua vigência, validade e eficácia!</p><button class="info">Referência</button>&nbsp;&nbsp;<button class="again">Ver novamente</button>');
      },
      dismiss: function() {
        clique.play();
        $('.content').fadeIn();
        return $('.dimmer').fadeOut();
      },
      init: function() {
        ini++;
        clique.play();
        if (ini === 1) {
          $('.ps p:nth-child(1), .init').fadeOut(function() {
            return $('.init').html('Avançar');
          });
          return $('.ps p:nth-child(2)').delay(400).fadeIn(function() {
            return $('.init').fadeIn();
          });
        } else if (ini === 2) {
          $('.init').addClass('start');
          $('.start').removeClass('init');
          $('.ps p:nth-child(2), .start').fadeOut();
          return $('.ps p:nth-child(3)').delay(400).fadeIn(function() {
            return $('.start').fadeIn();
          });
        }
      },
      start: function() {
        var i, j, k, l, len, len1, m, ref;
        j = 0;
        for (l = 0, len = data.length; l < len; l++) {
          i = data[l];
          $('.slides').append('<section> <div class="txt"><h2>' + i.tit + '</h2> <p>' + i.txt[0] + '</p></div> <img src="' + imgs[j] + '"> <div class="itens"></div> </section>');
          ref = i.txt;
          for (m = 0, len1 = ref.length; m < len1; m++) {
            k = ref[m];
            $('.slides section:nth-child(' + (j + 1) + ') .itens').append('<div class="i"></div>');
          }
          j++;
        }
        return func.dismiss();
      },
      slideshow: function($el) {
        clique.play();
        count = $el.index();
        $('.ctrl button').css({
          border: 'none'
        });
        $('.ctrl button:nth-child(' + ($el.index() + 1) + ')').css({
          border: '0.1em solid white'
        });
        $('.slides').animate({
          top: anima[$el.index()] + '%'
        }, 500, function() {
          return $('.slides section:nth-child(1) .txt p').html(data[count].txt[0]);
        });
        $('.itens .i').css({
          background: 'rgba(255,255,255,0.6)'
        });
        return $('.itens .i:nth-child(1)').css({
          background: 'rgba(255,255,255,1)'
        });
      },
      subItens: function($el) {
        clique.play();
        $('.itens .i').css({
          background: 'rgba(255,255,255,0.6)'
        });
        $('.itens .i:nth-child(' + ($el.index() + 1) + ')').css({
          background: 'rgba(255,255,255,1)'
        });
        $('.slides section:nth-child(' + (count + 1) + ') .txt p').css({
          marginLeft: '200%'
        });
        $('.slides section:nth-child(' + (count + 1) + ') .txt p').animate({
          marginLeft: '0'
        }, 800);
        $('.slides section:nth-child(' + (count + 1) + ') .txt p').html(data[count].txt[$el.index()]);
        if (count === 4 && $el.index() === 1) {
          return $('.endit').fadeIn();
        }
      }
    };
    $(document).on('click', '.init', function() {
      return func.init();
    });
    $(document).on('click', '.start', function() {
      return func.start();
    });
    $(document).on('click', '.help', function() {
      return func.help();
    });
    $(document).on('click', '.info', function() {
      return func.info();
    });
    $(document).on('click', '.end, .endit', function() {
      return func.end();
    });
    $(document).on('click', '.dismiss', function() {
      return func.dismiss();
    });
    $(document).on('click', '.ctrl button', function() {
      return func.slideshow($(this));
    });
    $(document).on('click', '.itens *', function() {
      return func.subItens($(this));
    });
    return $(document).on('click', '.again', function() {
      return location.reload();
    });
  });

}).call(this);
