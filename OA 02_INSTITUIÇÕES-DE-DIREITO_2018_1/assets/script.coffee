###
	       OA TEMPLATE 4.0
	----------------------------
	Desenvolvido em CoffeeScript

		Código: Fabiane Lima
###

# ----- Pré-carregamento das imagens ----- #
imgs =	[
			'assets/img/img1.png'
			'assets/img/img2.png'
			'assets/img/img3.png'
			'assets/img/img4.png'
			'assets/img/img5.png'
		]

preload = (imgs) ->
	counter = 0

	$(imgs).each ->
		$('<img />').attr('src', this).appendTo('body').css { display: 'none' }
		counter++

	if counter is imgs.length
		$('main').css { opacity: '1' }
		$('body').css { background: '#e7e7e7' }

$(window).on 'load', -> preload(imgs)


# ----- Funções e dados ----- #
$ ->
	ini = 0
	count = 0
	anima = [ 0, -100, -200, -300, -400 ]
	clique = new Audio('assets/clique.mp3')
	data =	[
						{
							tit: 'Validade da lei'
							txt: [
											'A lei é uma regra escrita, de força obrigatória, cujo descumprimento gera uma sanção. Devem ser observadas algumas condições quanto à forma e quanto à matéria da lei para saber se ela é válida:'
											'<strong>Ótica Formal</strong><br/>A validade sob a ótica formal depende do processo de criação da lei, que precisa estar em conformidade com o devido processo legislativo previsto na Constituição Federal. Se assim não for, ela não será válida.'
											'<strong>Ótica Material</strong><br/>A validade sob a ótica material refere-se à matéria, ao assunto tratado pela lei. Assim, existem assuntos que não podem ser objeto da lei. Também alguns assuntos são restritos a determinado ente da federação, então, se uma lei for aprovada pelo Estado do Paraná e tratar de assunto de competência do Município de Curitiba, a lei não será válida.'
									 ]
						}
						{
							tit: 'Vigência da lei'
							txt: [
											'Uma lei deve ser elaborada, promulgada e publicada. Ao ser elaborada, a lei é encaminhada para o legislativo (quem aprova uma lei) por meio de um projeto de lei.'
											'Depois ela é submetida a aprovação daquele órgão que irá votar o projeto. Se o projeto de lei for aprovado, passa a ser uma lei.'
											'Algumas leis têm tanta repercussão na vida das pessoas que não podem produzir efeitos logo que são publicadas, ou seja, não podem passar a ter vigência imediatamente.'
											'A lei é encaminhada para o executivo (aquele que promulga e publica a lei). Só depois de publicada é que a lei passa a ser obrigatória para todas as pessoas.'
									 ]
						}
						{
							tit: 'Eficácia da lei'
							txt: [
											'Enquanto a lei não está em vigor, ou seja, no período de vacância, “a lei como ato jurídico, existe, é válida, mas não é eficaz. A eficácia da lei, isto é, seus efeitos plenos, só ocorrem com sua entrada em vigor” (VENOSA, 2012, p. 108).'
											'Imagine que uma lei publicada em 1 de janeiro de 2015 tenha no seu corpo um artigo que determina que ela entrará em vigor em 1 ano. Neste caso, a lei teria vacatio legis expressa.'
											'Diante dessa situação, a lei, a partir do dia 2 de janeiro de 2015, é válida, mas não é eficaz, porque não está vigente ainda.'
									 ]
						}
						{
							tit: 'Revogação da lei'
							txt: [
											'Uma lei válida torna-se eficaz, ou seja, passa a produzir efeitos quando entra em vigor. Mas até quando ela ficará vigente?'
											'Até que outra lei a revogue ou a modifique, ela ainda estará produzindo efeitos no mundo jurídico.'
											'Denomina-se ab-rogação quando a lei é revogada integralmente e derrogação quando é revogada parcialmente.'
									 ]
						}
						{
							tit: 'Princípio da irretroatividade da lei'
							txt: [
											'Uma vez em vigor, a lei terá plena eficácia, total aplicabilidade, mas a Constituição Federal, no art. 5º, inciso XXXVI, garante que uma lei nova não pode prejudicar o direito adquirido, ato jurídico perfeito e coisa julgada (VENOSA, 2012).'
											'Se uma pessoa se aposentou com 60 anos em janeiro de 2015 e em fevereiro do mesmo ano uma lei nova determinou que a idade mínima para se aposentar é com no mínimo 70 anos, como fica a situação da pessoa que já se aposentou com menos idade? Ela continuará aposentada porque já tinha direito adquirido.'
									 ]
						}
			]
	func =
		info: ->
			clique.play()
			$('.modal').html('<h1>Referência</h1><p>VENOSA, Sílvio de Salvo. <b>Direito civil</b>. 12. ed. São Paulo: Atlas, 2012. v. 1.</p><button class="end">Voltar</button>')

		end: ->
			clique.play()
			$('.content').fadeOut()
			$('.dimmer').delay(200).fadeIn()
			$('.modal').html('<h1>Finalizando...</h1><p>Recapitulamos algumas características importantes das leis, principal fonte do direito, sua vigência, validade e eficácia!</p><button class="info">Referência</button>&nbsp;&nbsp;<button class="again">Ver novamente</button>')

		dismiss: ->
			clique.play()
			$('.content').fadeIn()
			$('.dimmer').fadeOut()

		init: ->
			ini++
			clique.play()

			if ini is 1
				$('.ps p:nth-child(1), .init').fadeOut -> $('.init').html('Avançar')
				$('.ps p:nth-child(2)').delay(400).fadeIn -> $('.init').fadeIn()

			else if ini is 2
				$('.init').addClass('start')
				$('.start').removeClass('init')
				$('.ps p:nth-child(2), .start').fadeOut()
				$('.ps p:nth-child(3)').delay(400).fadeIn -> $('.start').fadeIn()

		start: ->
			j = 0

			for i in data
				$('.slides').append('
					<section>
						<div class="txt"><h2>' + i.tit + '</h2>
						<p>' + i.txt[0] + '</p></div>
						<img src="' + imgs[j] + '">
						<div class="itens"></div>
					</section>
				')

				for k in i.txt
					$('.slides section:nth-child(' + (j + 1) + ') .itens').append('<div class="i"></div>')

				j++

			func.dismiss()

		slideshow: ($el) ->
			clique.play()
			count = $el.index()
			$('.ctrl button').css { border: 'none' }
			$('.ctrl button:nth-child(' + ($el.index() + 1) + ')').css { border: '0.1em solid white' }

			$('.slides').animate { top: anima[$el.index()] + '%' }, 500, ->
				$('.slides section:nth-child(1) .txt p').html(data[count].txt[0])

			$('.itens .i').css { background: 'rgba(255,255,255,0.6)' }
			$('.itens .i:nth-child(1)').css { background: 'rgba(255,255,255,1)' }

		subItens: ($el) ->
			clique.play()
			$('.itens .i').css { background: 'rgba(255,255,255,0.6)' }
			$('.itens .i:nth-child(' + ($el.index() + 1) + ')').css { background: 'rgba(255,255,255,1)' }
			$('.slides section:nth-child(' + (count + 1) + ') .txt p').css { marginLeft: '200%' }
			$('.slides section:nth-child(' + (count + 1) + ') .txt p').animate { marginLeft: '0' }, 800
			$('.slides section:nth-child(' + (count + 1) + ') .txt p').html(data[count].txt[$el.index()])

			if count is 4 and $el.index() is 1 then $('.endit').fadeIn()


# ----- Eventos ----- #
	$(document).on 'click', '.init', -> func.init()
	$(document).on 'click', '.start', -> func.start()
	$(document).on 'click', '.help', -> func.help()
	$(document).on 'click', '.info', -> func.info()
	$(document).on 'click', '.end, .endit', -> func.end()
	$(document).on 'click', '.dismiss', -> func.dismiss()
	$(document).on 'click', '.ctrl button', -> func.slideshow $(this)
	$(document).on 'click', '.itens *', -> func.subItens $(this)
	$(document).on 'click', '.again', -> location.reload()
